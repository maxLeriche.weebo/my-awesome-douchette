﻿using System;
using System.Data.SQLite;

namespace Doucehtte
{
    public static class Test_Douchette
    {
       
        /// <summary>
        /// test if the string has the good form
        /// </summary>
        /// <param name="Chaine_lue">the string to test</param>
        /// <returns>true if the form has the good format, false if not</returns>
        public static bool Verification(string Chaine_lue)
        {
            string[] Chaine_cut = Chaine_lue.Split("-");
            if(Chaine_cut.Length==3 && VerifType(Chaine_cut[0])&& VerifYear(Chaine_cut[1])&&VerifPro(Chaine_cut[2]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        //test the type (first pair of 4 char)
        private static bool VerifType(string type)
        {
            if(type.Length==3)
            {
                return true;
            }
            return false;
        }

        //test the year (second pair of 4 int)
        private static bool VerifYear(string year)
        {
            if(year.Length==4)
            {
                if (Int32.TryParse(year, out int test))
                {
                    return true;
                }
            }
            return false;
        }

        //test the product (third pair of 4 int)
        private static bool VerifPro(string product)
        {
            if(product.Length==4)
            {
                int test;
                if (Int32.TryParse(product,out test))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
