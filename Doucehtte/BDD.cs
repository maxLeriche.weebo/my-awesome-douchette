﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;


namespace Doucehtte
{
    public class BDD
    {
        SQLiteConnection Cursor;
        string path = "local.sqlite3";


        /// <summary>
        /// Create the database connection
        /// </summary>
        /// <param name="DB_Path"> Path to the database</param>
        public BDD(string DB_Path)
        {
            try
            {
                this.path =  DB_Path;
                bool temporary = pathexists(); //test if the past exist 
                Cursor = new SQLiteConnection("Data Source=" + DB_Path + "; Version = 3;"); //create the connexion
                Cursor.Open(); //open the cursor
                if (!temporary)  //if the path doesnt exist create the tab
                {
                    
                    CreaTab(this.path);
                }

            }
            catch (Exception ee)
            {
                Exception temporary = new Exception("ERROR ACCESSING DATA BASE:" + ee.Message);
                throw temporary;
            }
        }

        /// <summary>
        /// Create the database connection
        /// </summary>
        public BDD()
        {
            try
            {
                bool temporary = pathexists(); //test if the the path exist
                
                Cursor = new SQLiteConnection("Data Source=" + this.path + "; Version = 3;"); //create the connexion
                Cursor.Open();//open the cursor
                if (!temporary)//if the path doesnt exist create the tab
                {
                    
                    CreaTab(this.path);
                }
            }

            catch (Exception ee)
            {
                Exception temporary = new Exception("ERROR ACCESSING DATA BASE:" + ee.Message);
                throw temporary;
            }
        }

        /// <summary>
        /// Close the connection between the programme and the sqlite3 file
        /// </summary>
        public void Disconnect()
        {
            Cursor.Close();
        }

        /// <summary>
        ///  Test if hte path exists
        /// </summary>
        /// <returns>return if true or not</returns>
        private bool pathexists()
        {

            if (File.Exists(path))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Inset data into the database 
        /// </summary>
        /// <param name="type">Type of the object scanned</param>
        /// <param name="year">production year of the object</param>
        /// <param name="product">Product id</param>
        /// <returns></returns>
        public bool insert(string type, string year, string product)
        {

            try
            {
                SQLiteCommand insert = new SQLiteCommand(Cursor);
                insert.CommandText = ("insert into data (TYPE,YEAR,REF) Values (@type, @year, @product);");
                insert.Parameters.AddWithValue("@type",type);
                insert.Parameters.AddWithValue("@year",year);
                insert.Parameters.AddWithValue("@product",product);
                insert.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                Exception tothrow = new Exception("ERREUR during the insert" + ee.Message);
            }

            return true;

            
        }


        /// <summary>
        /// create the file
        /// </summary>
        /// <param name="path">path to the db to create</param>
        /// <returns>return if the tab has realy been create</returns>
        private bool CreaTab(string path)
        {
            try
            {
                SQLiteCommand create = new SQLiteCommand(Cursor);
                create.CommandText = "CREATE TABLE data (TYPE TEXT,YEAR TEXT,REF INTEGER, [index] INTEGER PRIMARY KEY AUTOINCREMENT);";
                create.ExecuteNonQuery();
            }
           catch
            {
                return false;
            }
            return true;
        }
    }


}
