﻿using System;
using Doucehtte;

namespace ConsoleApp1
{
    class Program
    {
        static void test_imput(string swap,BDD test)
        {
            if (Test_Douchette.Verification(swap))
            {
                string[] cut = swap.Split('-');
                test.insert(cut[0], cut[1], cut[2]);
                Console.WriteLine("Ecriture d'une ligne");
            }
            else
            {
                Console.WriteLine("n'écrit pas");
            }
        }

        static void Main(string[] args)
        {
            BDD test = new BDD();
            string swap = "test";
            test_imput(swap, test);
            test_imput("PER-2019-5869",test);
            test_imput("PER-2019-586A", test);
            test_imput("PER-2019-5869", test);

            test.Disconnect();
        }
    }
}
